# Finesse 3 documentation

These directories contain the reStuctured Text (reST) files (`.rst` extension) making up
the documentation for the `finesse` package. For information on how to build the
documentation, see the [existing
documentation](https://finesse.docs.ligo.org/finesse3/developer/documenting.html#building-the-documentation).
