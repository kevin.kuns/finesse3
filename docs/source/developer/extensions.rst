.. include:: /defs.hrst
.. _extension_dev:

=====================================
Contributing to the editor extensions
=====================================

The various editor extensions have different structures and packaging requirements.
Where applicable, instructions for contributing can be found in each project's README.
Specifics for packaging and distribution can be found here.

.. contents:: Table of Contents
    :local:
    :depth: 1


JupyterLab
==========

Repository: https://git.ligo.org/finesse/katscript-jupyterlab

The JupyterLab extension is distributed in two places; via pip and npm. New versions
should be published at both locations. You need to have `nodejs
<https://nodejs.org/en/>`__ installed to work with the extension.

pip
---

First, make sure you have an account on https://pypi.org. An existing collaborator can
then invite you to be a maintainer, by going to the project's `collaboration page
<https://pypi.org/manage/project/katscript-jupyterlab/collaboration>`_ and entering your
username.

To publish an update, run the following from the repository::

    $ pip wheel . -w dist

This will create a folder named ``dist`` and build a wheel of the extension there. You
can then publish this to PyPI with twine::

    $ twine upload dist/katscript_jupyterlab-*.whl

After following the prompt to login, your update will be published.

npm
---

The ``npm`` package is published under the `gwoptics organisation
<https://www.npmjs.com/org/gwoptics>`_. An existing collaborator can invite you to join
the organisation from the `members page
<https://www.npmjs.com/settings/gwoptics/members>`_, either by email or username. Either
way, you'll need to create an account on https://www.npmjs.com.

To publish an update, you'll first need to log in to ``npm`` from the command-line::

    $ npm login

Next, from the repository, run::

    $ npm run build
    $ npm publish

Your update should then be immediately available from the JupyterLab extension manager.

.. warning::

    Files published to https://npmjs.com are **permanent**; you cannot unpublish a
    package more than 24 hours after it was published, so take care.


Visual Studio Code
==================

Repository: https://git.ligo.org/finesse/katscript-vscode

The Visual Studio Code extension is published in two places: the `Visual Studio
Marketplace <https://marketplace.visualstudio.com/items?itemName=gwoptics.katscript>`_,
which is used by Visual Studio Code, and the open-source `Open VSX
<https://open-vsx.org/extension/gwoptics/katscript>`_, which is used by `VSCodium
<https://vscodium.com/>`_ and other editors. New versions should be published at both
locations. To publish at either location, you'll need to have `nodejs
<https://nodejs.org/en/>`_ installed. The initial set-up for both locations is fairly
involved.

Visual Studio Marketplace
-------------------------

The Visual Studio Marketplace extension is published under the `gwoptics publisher
<https://marketplace.visualstudio.com/publishers/gwoptics>`_, which is tied to the
corresponding gwoptics Azure DevOps organisation. First, an existing member must invite
you to the Azure organisation from the `organisation settings page
<https://dev.azure.com/gwoptics/_settings/users>`_. You then need to obtain a Personal
Access Token, as described in the `extension guide
<https://code.visualstudio.com/api/working-with-extensions/publishing-extension>`_.

Next, you need to be invited to the gwoptics publisher, which an existing member can do
from the `publisher management page
<https://marketplace.visualstudio.com/manage/publishers/gwoptics>`_. An "Invalid Domain"
error may occur when adding a new user, in which case the existing member will have to
contact Microsoft support from the "Contact Microsoft" link in the top-right. They will
need the ID of the new user, which can be obtained as described in `this stackoverflow
answer <https://stackoverflow.com/a/56874572/4541633>`_, so you may as well send that as
part of the request for help.

To actually publish an update, follow the steps in the `extension guide
<https://code.visualstudio.com/api/working-with-extensions/publishing-extension>`_.
You'll need your Personal Access Token to log in. These basically amount to running the
following from the repository::

    $ npx vsce login gwoptics
    $ npx vsce publish

Open VSX
--------

The Open VSX extension is again under the gwoptics namespace. The following steps are
the same as those in the `publishing guide
<https://github.com/eclipse/openvsx/wiki/Publishing-Extensions>`_.

First, `create a GitHub account <https://github.com/join>`_ if you don't have one. Next,
`create an Eclipse account <https://accounts.eclipse.org/user/register>`_, filling in
the *GitHub Username* field with the correct name. You can then log in via
https://open-vsx.org/ using this GitHub account. From your `profile page
<https://open-vsx.org/user-settings/profile>`_, click *Log in with Eclipse* to authorize
Eclipse to access your account. Next, click the newly-added *Show Publisher Agreement*
button on your profile page, and click `Agree` if you do. An existing member can then
add your username to the namespace from their `namespaces settings page
<https://open-vsx.org/user-settings/namespaces>`_.

Once you have an account, you'll have to generate an Access Token from your `settings
page <https://open-vsx.org/user-settings/tokens>`_. You can then publish the extension
by running the following from the repository, replacing ``<token>`` with your Access
Token::

    $ npx ovsx publish -p <token>


Vim
===

Repository: https://git.ligo.org/finesse/katscript-vim

The Vim extension can be installed in the usual ways, such as with vim-plug or Vim's
built-in package management. No packaging work is required, as it's installed straight
from the git repository.


Pygments
========

The Pygments extension is installed along with |Finesse| by defining the
``pygments.lexers`` entry point in :source:`setup.py </setup.py>`, so no further
packaging work needs to be done. The actual extension is defined in
:source:`script/highlighter.py`, and is described there.
