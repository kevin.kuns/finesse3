.. include:: ../defs.hrst

.. _continuous_integration:

Continuous integration
----------------------

The |Finesse| project makes use of GitLab's `continuous integration
<https://docs.gitlab.com/ee/ci/>`__ feature to automatically build, test and deploy
|Finesse| on various environments.

The primary way in which continuous integration is used in the |Finesse| project is to
run tests on different target platforms to catch bugs there, especially those that don't
appear in a developer's :ref:`editable install <setting_up_finesse_dev_env>`. Other uses
are to build and deploy the documentation, and to generate statistics regarding the
project such as its test coverage. The pipeline run history is also useful to help
determine which changes to |Finesse| or its dependencies introduced some bug or change
in behaviour.

The continuous integration pipeline runs on GitLab automatically whenever code is pushed
to the upstream |Finesse| repository. If you have configured your account for it, GitLab
will send an email whenever a pipeline triggered by your push fails. You can inspect the
console output from the failed job by clicking the link in the email, or browsing to the
*CI/CD* section of the GitLab project.

Configuring the pipeline
========================

The :source:`.gitlab-ci.yml </.gitlab-ci.yml>` file in the project root defines the
continuous integration pipeline. See the `syntax reference
<https://git.ligo.org/help/ci/yaml/index>`__ for more information on how this must be
formatted. This file can also be edited using the GitLab `web interface
<https://git.ligo.org/finesse/finesse3/-/ci/editor>`__.

A `linting tool <https://git.ligo.org/finesse/finesse3/-/ci/lint>`__ is provided as part
of the GitLab project to ensure there are no syntax errors or issues before committing
changes to this file.

Pushing code without triggering the pipeline
============================================

GitLab supports `skipping the pipeline
<https://docs.gitlab.com/ee/user/project/push_options.html>`__ via git's `push options
<https://git-scm.com/docs/git-push#Documentation/git-push.txt--oltoptiongt>`__ flag.
This can be used to help save needless computation for minor changes that don't need the
full build and test suite to run:

.. code-block:: console

    $ git push -o=ci.skip

The pipeline will then appear in GitLab as "skipped".
