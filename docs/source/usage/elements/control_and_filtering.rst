.. include:: /defs.hrst

=====================
Control and filtering
=====================

.. kat:element:: lock

.. kat:element:: degree_of_freedom

.. kat:element:: amplifier

.. kat:element:: filter_zpk

.. kat:element:: filter_butter

.. kat:element:: filter_cheby1

.. kat:element:: zpk_actuator
