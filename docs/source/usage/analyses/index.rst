.. include:: /defs.hrst

.. _analyses:

Actions, Analyses, and Solutions
================================

The way in which simulations are defined and run is significantly different in |Finesse|
3. Users familiar with ``xaxis`` in |Finesse| 2 can happily continue using it in version
3. However, for users wishing to perform more complicated modelling tasks, actions,
analyses, and solutions provide a new interface to take your simulation modelling to the
next level. These enable more advanced physics simulations with greater computational
efficiency, all with a simple to use interface.

The introduction is a must-read for getting the most out of |Finesse| 3. More technical
details can be found in other sections for those users wanting to know more.

.. toctree::
    intro
    groups
    axes
    physics
    utilities
