.. include:: /defs.hrst
.. _key_concepts:

.. seealso:: :ref:`migration`

Key concepts
------------

There are three distinct layers in the |Finesse| interaction that will be referred to
throughout this documentation:

The model
    a particular optical configuration or layout (see :mod:`finesse.model`)

The simulation
    when a |Finesse| model is executed to generate a particular output (see
    :mod:`finesse.simulations`)

The solution
    data generated by a particular simulation (see :mod:`finesse.solutions`)

.. _detectors_probes:

Detectors and probes
********************

A *detector* is defined as a component that converts an optical input into an electrical
output, this can be a simple photo diode measuring a light power, or a complex device
with multiple (optical) inputs and signal processing such as demodulation. A detector
will have input and output nodes, will be computed as part of the matrix representing
the model. It can only perform linear operations (for signal processing) and therefor
applies only to of AC signals. For example, a detector cannot measure the light power at
DC. However, detectors can be useful for modelling signal propagation in mixed systems
(optical, electrical, mechanical).

A *probe* is a user-defined output of any simulation result, for example a specific
light field amplitude or an electrical signal. A probe can include specific
post-processing of the simulation output, for example, summing up powers in optical
field components, or demodulation. A probe is not part of the model itself and does not
block the light or signal path and it can therefor include non-linear post-processing.
Many probes can be connected to each note without interfering with the model or each
other.

.. todo:: DC and AC signal nomenclature


The model graph
***************
Any |Finesse| mode is defined as a nodal network, which is stored in a graph.

The simulation matrix
*********************

|Finesse| uses a sparse matrices to perform the main simulation task.

.. todo::

    probably the matrix part is not required here. This section should be limited to key
    concepts that a new user would need to run and understand an example. Internals such
    as the matrix can be handled elsewhere.
