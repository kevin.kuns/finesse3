.. include:: /defs.hrst
.. _extensions:

=================
Editor extensions
=================

A number of extensions to text editors and IDEs have been written to ease working with
|Finesse| and KatScript code. This page contains information and installation
instructions for each of them.

.. contents:: Table of Contents
    :local:

JupyterLab
==========

The JupyterLab extension provides syntax highlighting of KatScript within Python
triple-quoted strings that start with a special marker: ``#kat`` for |Finesse| 3 style
KatScript, or ``#kat2`` for |Finesse| 2 style KatScript.

.. cssclass:: nobg
.. figure:: images/katscript-jupyterlab-example.*
    :align: center
    :width: 80%

You can install the JupyterLab extension either via a Python packaging tool such as
`pip <https://pip.pypa.io/en/stable/installing/>`__ or the JupyterLab extension manager.
To install via `pip`, run::

    pip install katscript-jupyterlab

To install via the JupyterLab extension manager, enable it and search for KatScript:

.. cssclass:: nobg
.. figure:: images/katscript-jupyterlab-install.*
    :align: center
    :width: 80%

Visual Studio Code
==================

The Visual Studio Code extension provides syntax highlighting of KatScript in ``.kat``
files. It defaults to |Finesse| 3 style KatScript; to highlight |Finesse| 2 style
KatScript, start the file with the comment ``#kat2``.

.. cssclass:: nobg
.. figure:: images/katscript-vscode-example.*
    :align: center
    :width: 80%

You can install the Visual Studio Code extension from the built in extension manager:

.. cssclass:: nobg
.. figure:: images/katscript-vscode-install.*
    :align: center
    :width: 50%

Vim
===

The Vim extension provides syntax highlighting of KatScript in ``.kat``
files. It defaults to |Finesse| 3 style KatScript; to highlight |Finesse| 2 style
KatScript, start the file with the comment ``#kat2``.

.. cssclass:: nobg
.. figure:: images/katscript-vim-example.*
    :align: center
    :width: 80%

You can install the Vim extension using your preferred method of managing extensions
from the `git repository <https://git.ligo.org/finesse/katscript-vim>`_.

Pygments
========

The Pygments extension is installed along with |Finesse|, and contains two highlighters.
The first applies to ``.kat`` files, and requires no further setup - Pygments will
automatically use the right highlighter. The second applies to KatScript within Python
triple-quoted strings that start with ``#kat``, and must be specified manually when
using Pygments:

.. cssclass:: nobg
.. figure:: images/katscript-pygments-example.*
    :align: center
    :width: 90%
