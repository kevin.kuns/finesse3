.. include:: ../../../defs.hrst
.. _source-installation:

Installing from source
======================

Installing |Finesse| from source means you can get access to mulitple versions,
including the latest development features. This method should only be used by people who
want to run latest development release or want to make changes to the way |Finesse|
operates. Regular users should use :ref:`installation-pypi` or :ref:`installation-conda`
instead.

If you are planning on making changes to |Finesse| please read the developer
documentation.

The |Finesse| source code is managed using the versioning control software `git
<https://git-scm.com/>`__.

It's difficult to provide build instructions for all possible systems and
configurations. Below we detail the most common ways we know of or use ourselves as
developers for building |Finesse|. If you run into issues please contact us.

.. warning::

    The following descriptions may not include any required activation step for the
    particular Python environment that |Finesse| will be installed into. We assume if
    you are installing from source you will be activating a Python environment of your
    own choosing and you know how to do this. If you do not, we recommend using `pip and
    virtualenv
    <https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/>`__
    or `Conda environments
    <https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html>`__.

.. toctree::

    windows
    mac
    linux

.. _source-rebuild:

Rebuilding |Finesse| after installing from source
-------------------------------------------------

Once you have installed |Finesse| from source you can easily get updates and rebuild it
by executing the following commands from the |Finesse| repository directory:

.. code-block:: console

    git pull
    make clean
    make

This should rebuild |Finesse| and can take some time. For small changes to the source
you can omit ``make clean``.

Occasionally changes to the build tooling will require a complete reset of the local
clone of the |Finesse| repository. To do this, the command ``make realclean`` can be
used. Note that after running this, you will have to rerun setup.

.. _source-change-version:

Changing |Finesse| versions after installing from source
--------------------------------------------------------

Switching versions in |Finesse| involves checking out a different git branch. The
process is similar to above when rebuilding, except you use ``git checkout`` beforehand.
It is recommended to use ``make clean`` when switching branches or commits.

.. code-block:: console

    cd finesse3
    git checkout [ref]
    git pull
    make clean
    make

You can replace ``[ref]`` with one of:

``master``
    The latest stable release.

``develop``
    The latest development release (not necesarily stable or runnable).

e.g. ``3.0a1``
    A particular tag.
