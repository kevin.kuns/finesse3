.. include:: ../../../defs.hrst
.. _osx-source-installation:

Mac OSX
=======

When installing on OSX you'll need to ensure you have xcode and its command line
utilities installed. Xcode can be installed from the app store. Once that is installed
open a terminal and install the command line utilities:

.. code-block:: console

    xcode-select --install

Use `MacPorts <https://www.macports.org/>`__ or `Brew <https://brew.sh/>`__ to install
the `suitesparse <https://people.engr.tamu.edu/davis/suitesparse.html>`__ library.
Alternatively you can also get it using `Conda
<https://anaconda.org/anaconda/suitesparse>`__ if you are using that anyway for
environment or package managing.

By default the above will install the latest `stable` version. See
:ref:`source-change-version` on how to change to different versions such as the most
current development release.

.. code-block:: console

    git clone https://git.ligo.org/finesse/finesse3.git
    cd finesse3
    pip install -e .
    python -c 'import finesse; print(finesse.__version__)'

See :ref:`source-rebuild` for details on how to rebuild |Finesse| after pulling latest
changes from the git repository.

Known problems
##############

OSX Catalina can sometimes thrown an error that it cannot find ``stdio.h``. This appears
to be a path setup issue. It can be fixed using

.. code-block:: console

    export CPATH='/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/include:/opt/local/include'
    export LIBRARY_PATH='/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/usr/lib:/opt/local/lib'
