.. include:: ../../../defs.hrst
.. _linux-source-installation:

Linux
=====

.. todo:: add other linux distros

Debian based systems
--------------------

Debian-based systems need the following packages:

.. code-block:: console

    apt install make gcc python3 python3-dev python3-pip git libsuitesparse-dev

Once the relevant system packages are installed simply clone the repository and build
|Finesse| as described below. By default the above will install the latest `stable`
version. See :ref:`source-change-version` on how to change to different versions such as
the most current development release.

.. code-block:: console

    git clone https://git.ligo.org/finesse/finesse3.git
    cd finesse3
    pip3 install -e .
    python3 -c 'import finesse; print(finesse.__version__)'

Installing |Finesse| can take some time as it needs to compile some extensions before it
can run. You should see no errors and the version number installed should be printed.

See :ref:`source-rebuild` for details on how to rebuild |Finesse| after pulling latest
changes from the git repository.
