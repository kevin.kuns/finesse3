.. include:: ../../../defs.hrst
.. _windows-source-installation:

Windows
=======

On Windows we recommend using Conda to install and build |Finesse|. It is possible to
build |Finesse| without using Conda but it is difficult and therefore not recommended.
|Finesse| must be built using GCC; it currently does not compile with MSVC and we are
not likely to support it in the near future. The `MSYS2 <https://www.msys2.org/>`__
build tools can instead be used to compile |Finesse| and these are easily installed via
Conda.

You must have Git installed to access the source code repository. You can install Git in
a variety of ways on Windows. The most common way is by `following these instructions
<https://git-scm.com/book/en/v2/Getting-Started-Installing-Git/>`__

From an `Anaconda Prompt` or `Miniconda Prompt` download the git repository into your
desired directory:

.. code-block:: console

    git clone -b master https://git.ligo.org/finesse/finesse3.git
    cd finesse3

The above will checkout the `stable` version of |Finesse| 3 in the ``master`` branch.
See :ref:`source-change-version` on how to change to different versions such as the most
current development release.

If you haven't already got an environment you would like to use |Finesse| in you can
create a new one using the following commands:

.. code-block:: console

    conda env create -n finesse3 -f environment-win.yml
    conda activate finesse3

If you have an environment you want to install |Finesse| in to use can install the build
dependencies using:

.. code-block:: console

    conda activate [your environment]
    conda update env --file environment-win.yml

|Finesse| should have now been built and installed into your environment. To quickly
test if it has worked, you can run the following:

.. code-block:: console

    python -c "import finesse; print(finesse.__version__)"

See :ref:`source-rebuild` for details on how to rebuild |Finesse| after pulling latest
changes from the git repository.
