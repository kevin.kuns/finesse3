.. include:: ../../defs.hrst
.. _installation-pypi:

Via PyPI (`pip`)
----------------

.. warning::

    The current version of |Finesse| hosted on PyPi is the
    first alpha release which is still underdevelopment.

When installing using `pip` this approach, it is recommended to install |Finesse| in a virtual environment.
This ensures that dependencies required for |Finesse| to run do not conflict with other
packages installed on your computer.

.. seealso::

    For more information on virtual environments, see the `Virtual Environments and
    Packages <https://docs.python.org/3/tutorial/venv.html>`__ section of the Python
    documentation.

|Finesse| is available on PyPI with the package name ``finesse``. There are many package
managers available for Python but the most common is `pip
<https://pip.pypa.io/en/stable/installing/>`__. Instructions for setting up a virtual
environment and installing |Finesse| via pip are given for each supported platform
below.

Linux / Mac OSX
~~~~~~~~~~~~~~~

To create a virtual environment, open up a terminal and run:

.. code-block:: console

    $ python -m venv finesse

Activate the virtual environment in your current terminal using:

.. code-block:: console

    $ source finesse/bin/activate

Install |Finesse| via pip with:

.. code-block:: console

    (finesse) $ pip install finesse

Windows
~~~~~~~

To create a virtual environment, open up a command prompt or powershell terminal and run:

.. code-block:: doscon

    C:\> py -m venv finesse

Activate the virtual environment in your current prompt using:

.. code-block:: doscon

    C:\> finesse\Scripts\activate.bat

Install |Finesse| via pip with:

.. code-block:: doscon

    (finesse) C:\> pip install finesse
