.. include:: ../defs.hrst
.. _getting_started:

Getting started
===============

Start here if you are new to |Finesse| or interferometer simulations in general. The
pages below will guide you through the installation process, briefly detail the main
concepts required for running a simulation, and walk you through a number of worked
examples to help you get used to using |Finesse|.

Users coming from |Finesse| 2 may wish to also read the :ref:`migration guide
<migration>` to understand what's different and new.

More advanced users will find the :ref:`usage` section a good starting point, and those
looking for a deeper understanding of the approach used by |Finesse| to simulate
interferometers will find the :ref:`physics` section useful.

.. toctree::
    :maxdepth: 1

    install/index.rst
    extensions
    key_concepts
    migrating_from_Finesse_2
    getting_help
