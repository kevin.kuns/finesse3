.. include:: ../defs.hrst
.. _getting_help:

Getting help
============

.. todo:: finish writing help section (for LSC members and for public)

Finesse and KatScript objects
-----------------------------

Refer to the rest of this documentation for information on the use of KatScript and
Finesse objects.

Interactive help
****************

The :func:`finesse.help` function accepts either a string or object as input and will
show corresponding help. If a string is provided, it is assumed to be a KatScript
:ref:`path <syntax_paths>` like ``m``, ``beamsplitter``, or ``bs.T``. Command names such
as ``xaxis`` are also supported.

This operates similarly to the Python built-in function :func:`help`: either a pager is
opened (if using a console; use :kbd:`PgUp` and :kbd:`PgDn` to navigate, and
:kbd:`q` to escape) or the help text is printed (if using a notebook).
