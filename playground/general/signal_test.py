"""
Tests a R=1 mirror that is being oscillated with fsig and looking
at the generated audio sidebands.
"""
from finesse.parse import KatParser
from pykat import finesse
import numpy as np

katfile = """
    l l1 1 0 n1
    s s1 0 n1 n2
    m m1 1 0 0 n2 n3
    s s2 0 n3 n4
    l l2 2 0 n4
"""

k = KatParser()
ifo = k.parse(katfile)

ifo.fsig = 1
ifo.homs.append("00")

carrier, signal = ifo.build()

with carrier, signal:
    # Solve the carrier fields
    # carrier.M().print_rhs()
    carrier.run()
    # carrier.M().print_rhs()

    signal.M().set_rhs(signal.field(ifo.m1.mech.z), 1)
    # signal.M().print_rhs()
    # Then solve the audio sidebands
    signal.run(carrier, fill_rhs=False, zero_rhs=False)  # use a manual RHS vector
    # signal.M().print_rhs()
    # signal.print_matrix()

print("Carrier:")
print(carrier.out / np.sqrt(2))

print("\nSignal:")
print(signal.out / np.sqrt(2))

kat = finesse.kat()
kat.parse(katfile)
kat.parse(
    """
    fsig sig m1 z 1 0 1

    ad ad1 $fs n1*
    ad ad2 $fs n1
    ad ad3 1 n4*
    ad ad4 1 n4
    noxaxis
    yaxis abs:deg
"""
)
out = kat.run()

print(out["ad1"])
print(out["ad2"])
print(out["ad3"])
print(out["ad4"])
