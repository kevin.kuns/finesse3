import finesse
from finesse.detectors import CCD

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s2 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01

gauss gL0 L0.p1.o q=(-2+3j)
modes maxtem=2
"""
)

model.add(CCD("ccd", model.ITM.p1.o, 3, 3, 300))

profile()
