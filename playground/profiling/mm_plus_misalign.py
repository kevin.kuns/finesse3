import finesse

if __name__ == "__main__":
    from _profile import profile
else:
    from . import profile


model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=inf
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10 xbeta=20e-6

modes maxtem=12

gauss gL0 L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i

xaxis ITM.phi lin -180 180 600

pd refl ITM.p1.o
pd circ ETM.p1.i
pd trns ETM.p2.o
"""
)


profile()

import pykat

base = pykat.finesse.kat()
base.verbose = False
base.parse(
    """
l L0 1 0 n0
s s0 1 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ETM Rcx 10
attr ETM Rcy 10
attr ETM xbeta 20u

maxtem 12

gauss* gL0 L0 n0 -0.2 2.5
cav FP ITM nITM2 ETM nETM1

xaxis ITM phi lin -180 180 600

pd refl nITM1
pd circ nETM1
pd trns nETM2
"""
)

profile("base.run()")
