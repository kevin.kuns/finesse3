import matplotlib.pyplot as plt

CODE = """
l L0 1 0 n0

s s1 0 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc -10
attr ETM Rc 10

maxtem 4

cav FP ITM nITM2 ETM nETM1

xaxis ETM xbeta lin 1u 100u 100

ad ad00 0 0 0 nITM1
ad ad10 1 0 0 nITM1
ad ad20 2 0 0 nITM1
ad ad30 3 0 0 nITM1
ad ad40 4 0 0 nITM1
#ad ad50 5 0 0 nITM1
#ad ad60 6 0 0 nITM1
#ad ad70 7 0 0 nITM1
#ad ad80 8 0 0 nITM1
"""

def finesse2():
    import pykat

    model = pykat.finesse.kat()
    model.verbose = False
    model.parse(CODE)
    model.parse("""
    yaxis log abs
    """)
    out = model.run()
    out.plot(filename="homdebug_cav_f2.png", show=False)
    return out

def finesse3():
    import finesse

    model = finesse.parse(CODE, True)
    ifo = model.model
    out = model.run()

    fig = plt.figure()
    for det in ifo.detectors:
        plt.semilogy(out.x1, abs(out[det.name]), label=f"HG{det.n}{det.m}")

    plt.xlim(1e-6, 100e-6)
    plt.legend()
    plt.savefig("homdebug_cav_f3.png")

    return out

outf2 = finesse2()
outf3 = finesse3()

def rel_err(x, y):
    return abs((x - y)/y)


fig = plt.figure()
for i in range(5):
    plt.plot(outf3.x1, rel_err(abs(outf2[f"ad{i}0"]), abs(outf3[f"ad{i}0"])), label=f"HG{i}0 error")
plt.xlabel("ITM xbeta [rad]")
plt.ylabel("Relative error")
plt.legend()

plt.savefig("homdebug_cav_relerr.png")
