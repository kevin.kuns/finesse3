import finesse

ifo = finesse.Model()
ifo.parse(
    """
l L0 P=1

s s2 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01

gauss gL0 L0.p1.o q=(-2+3j)
modes maxtem=2

ccd refl_im_amp ITM.p1.o xlim=1 ylim=1 npts=300
"""
)

out = ifo.run()
out.plot()
