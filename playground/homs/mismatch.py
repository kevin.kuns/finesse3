import finesse

finesse.plotting.init()

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=(-10)
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes(maxtem=4)

cav FP ITM.p2.o ITM.p2.i

xaxis(ITM.phi, lin, (-180), (180), 800)

pd refl ITM.p1.o
pd circ ETM.p1.i
pd trns ETM.p2.o
"""
)
ifo = model
ifo.modes(modes="x", maxtem=4)
ifo.add_all_ad(ifo.ETM.p1.i, f=ifo.L0.f.ref)
ifo.create_mismatch(ifo.ITM.p1.o, w0_mm=1, z_mm=10)

out = model.run()
print(ifo.last_trace)
out.plot(logy=True, figsize_scale=2)
