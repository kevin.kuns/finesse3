import numpy as np

import finesse
import finesse.components as fc
import finesse.detectors as fd

from finesse.analysis.actions import Noxaxis

model = finesse.Model()
model.modes(maxtem=1)
L0_P = 1
model.add(fc.Laser("L0", P=L0_P))
model.add(fc.Nothing("END"))
model.connect(model.L0.p1, model.END.p1, L=1)
model.L0.p1.o.q = -1.4 + 0.8j
model.L0.tem(1, 0, 0.5)
model.L0.tem(0, 1, 0.25)

model.add(fd.AmplitudeDetector("ad00", model.L0.p1.o, 0, 0, 0))
model.add(fd.AmplitudeDetector("ad01", model.L0.p1.o, 0, 0, 1))
model.add(fd.AmplitudeDetector("ad10", model.L0.p1.o, 0, 1, 0))

out = model.run(Noxaxis())

assert(np.isclose(0.25*L0_P, np.abs(out["ad01"])**2))
assert(np.isclose(0.5*L0_P, np.abs(out["ad10"])**2))
assert(np.isclose(L0_P, np.abs(out["ad00"])**2))
