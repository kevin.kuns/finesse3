import numpy as np
import finesse

finesse.plotting.init()

model = finesse.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes even maxtem=4

gauss gL0 L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i
"""
)
ifo = model.model

Msol = finesse.analysis.abcd(
    ifo, path=ifo.path(ifo.ITM.p2.o, ifo.ITM.p2.i), direction="x", symbolic=True
)

q = ifo.FP.q
q2 = finesse.gaussian.transform_beam_param(Msol.M, q)

ETM_Rcx = np.linspace(8, 20, 100)
CAV_L = np.linspace(1, 2, 100)
ws = q2.w.eval(ETM_Rcx=ETM_Rcx, CAV_L=CAV_L)


import matplotlib.pyplot as plt

plt.imshow(
    ws, extent=[ETM_Rcx.min(), ETM_Rcx.max(), CAV_L.min(), CAV_L.max()], aspect="auto"
)
plt.show()
