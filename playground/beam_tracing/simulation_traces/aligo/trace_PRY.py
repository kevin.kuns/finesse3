from finesse.parse import parse

from pykat import finesse
from pykat.tools.detecting import all_bp_detectors

PRY_MODEL = r"""
l L0 125 0.0 0.0 ni

s sPRCin 0.4135 ni nPRM1

const nsilica 1.44963098985906
const Mloss 37.5u
const phi_BS 0
const phi_PRM 0.0
const phi_ITMY 0.0
const TLY_f 34.5k         # Thermal lens ITMX

%%% FTblock PRC
# PRM
# AR surface
m2 PRMAR 0 40u 0 nPRM1 nPRMs1
# Substrate
s sPRMsub1 0.0737 $nsilica nPRMs1 nPRMs2
# HR surface
m1 PRM 0.03 8.5u $phi_PRM nPRMs2 nPRM2
attr PRM Rc 11.009
# Distance between PRM and PR2
s lp1 16.6107 nPRM2 nPR2a
# PR2
bs1 PR2 250u $Mloss 0 -0.79 nPR2a nPR2b nPOP nAPOP
attr PR2 Rc -4.545
# Distance from PR2 to PR3
s lp2 16.1647 nPR2b nPR3a
# PR3
bs1 PR3 0 $Mloss 0 0.615 nPR3a nPR3b dump dump
attr PR3 Rc 36.027
# Distance from PR3
s lp3 19.5381 nPR3b nPRBS

###########################################################################
%%% FTend PRC

%%% FTblock BS
###########################################################################
# BS beamsplitter
##------------------------------------------------------------
## BS
##                             ^
##                  to IMY     |
##                             |      ,'-.
##                             |     +    `.
##                        nYBS |   ,'       :'
##      nPR3b                  |  +i1      +
##         ---------------->    ,:._  i2 ,'
##    from the PRC       nPRBS + \  `-. + nXBS
##                           ,' i3\   ,' --------------->
##                          +      \ +     to IMX
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nSRBS
##                             -   |
##                                 |to the SRC
##                                 |
##                                 v
##------------------------------------------------------------
bs1 BS 0.5 $Mloss $phi_BS 45 nPRBS nYBS nBSi1 nBSi3
s BSsub1 0.0687 $nsilica nBSi1 nBSi2
s BSsub2 0.0687 $nsilica nBSi3 nBSi4
bs2 BSAR1 50u 0 0 -29.195 nBSi2 dump14 nXBS nPOX
bs2 BSAR2 50u 0 0 29.195 nBSi4 dump15 nSRBS dump16

%%% FTend BS

# Distance from beam splitter to Y arm input mirror
s ly1 5.0126 nYBS nITMY1a

lens ITMY_lens $TLY_f nITMY1a nITMY1b
s sITMY_th2 0 nITMY1b nITMY1

# Y arm input mirror
m2 ITMYAR 0 20u 0 nITMY1 nITMYs1
s ITMYsub 0.2 $nsilica nITMYs1 nITMYs2
m1 ITMY 0.014 $Mloss $phi_ITMY nITMYs2 nITMY2
attr ITMY Rc -1934

cav cavPRY PRM nPRM2 ITMY nITMYs2
"""

model = parse(PRY_MODEL, legacy=True)
print(model.beam_trace())

base = finesse.kat()
base.verbose = False
base.parse(PRY_MODEL)
base.parse(all_bp_detectors(base))
base.parse(
    """
noxaxis
yaxis re:im
"""
)
out = base.run()
print("\nFinesse 2 trace results:")
print("--------------------------")
for probe in base.detectors.keys():
    print(probe[2:] + " qx = {}".format(out[probe]))
