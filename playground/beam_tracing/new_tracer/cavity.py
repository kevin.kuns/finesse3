import finesse

model = finesse.Model()
model.parse(
    """
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=-10
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

cav FP ITM.p2.o
gauss gL0 L0.p1.o w0=1.2m z=-1.2
"""
)
trace = model.beam_trace()
print(trace)
