import finesse

# finesse.LOGGER.setLevel("INFO")
model = finesse.parse(
    """
l l1 100 0 nin
s sin 0 nin n1_2

#######
# PRC #
#######
m mPRC 0.85 0.15 0 n1_2 nPRC1
attr mPRC Rc -2085.6

s sPRC1 5 nPRC1 nBS1

bs bs1 0.5 0.5 0 45 nBS1 nBS4 nBS3 nBS2

s sPRC3 5 nBS3 nPRC3
s sPRC4 5 nBS4 nPRC4

#####
# X #
#####
m mITM3  .1 0 90 nPRC3 nCav3
attr mITM3 Rc -2.076k

s sCav3 4000 nCav3 nETM3

m mETM3  1 0 0 nETM3 dump
attr mETM3 Rc 2.076k

#####
# Y #
#####
m mITM4  .1 0 90 nPRC4 nCav4
attr mITM4 Rc -2.076k

s sCav4 4000 nCav4 nETM4

m mETM4  1 0 0 nETM4 dump
attr mETM4 Rc 2.076k


##########
# Output #
##########
ad ad01 0 1 0 nBS2
ad ad02 0 2 0 nBS2
ad ad03 0 3 0 nBS2
ad ad04 0 4 0 nBS2

modes maxtem=4

cav arm3 mITM3 nCav3 mETM3 nETM3
cav arm4 mITM4 nCav4 mETM4 nETM4

cav PRC1 mPRC nPRC1 mITM4 nPRC4
cav PRC2 mPRC nPRC1 mITM3 nPRC3

xaxis mITM3 phi lin -30 120 200
yaxis lin abs
""",
    True,
)
ifo = model.model

out = model.run()

ifo.print_mismatches()
out.plot()
