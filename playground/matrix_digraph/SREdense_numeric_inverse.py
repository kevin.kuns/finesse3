# -*- coding: utf-8 -*-
# flake8: noqa
# Code originally from phasor2, Lee McCuller (Apache license v2.0)
# modified to include some additional documentation
"""
"""
import collections
import numpy as np
import scipy.linalg

from .np import broadcast_shapes

from .lin_op import (
    DeferredMatrixStore,
    MatrixStoreTypes,
)


def SREdense_numeric_inverse(
    seq, req, edges, inputs, outputs, node_order=None, node_sizes=None, metamat=None,
):
    """
    This is a matrix inversion routine starting from a digraph of dense matrices
    The edges of the digraph are scattering-matrix couplings and the nodes the
    physical field strengths on surfaces/components.

    This version of the algorithm merely constructs a large dense intermediate
    representation, and then calls numpy or scipy matrix inversion routine against
    it, then re-assembles the output into digraph form. It uses the same interface
    (and is indeed a subroutine) of the fully-accelerated form.

    The inputs are

    seq: a dictionary of sets, mapping nodes to a set of outgoing edges. Generally
    constructed using collections.defaultdict(set).
    This represents fill-in amongst rows of the matrix

    req: a dictionary of sets, mapping nodes to their incoming edges. This may
    be constructed from seq if needed (if req is None on input).

    node keys may be any hashable python type.

    edges: a dictionary containing the mapping of edges to matrices,
    {(node_fr, node_to) : matrix}. NOTE, the from-to convention is opposite the
    usual [row, col] mapping usual for matrices.

    inputs: a list or set of used input nodes. Only nodes in this set will
    have columns (from) nodes in the inverse

    outputs: a list or set of used output nodes. Only nodes in this set will
    be present in the rows (to-nodes) of the inverse.

    metamat: allows the user to provide a backing-store matrix to populate.
    Useful primarily for debugging.

    returns: a seq, req, edges tuple of the matrix inverse
    """
    if req is None:
        req = collections.defaultdict(set)
        for cnode, sset in seq.items():
            # rnode for row
            for rnode in sset:
                req[rnode].add(cnode)

    # then it is a trivial inversion!
    if not inputs or not outputs:
        return dict(), dict(), dict()

    inputs = set(inputs)
    outputs = set(outputs)
    nodes = inputs | outputs

    scalar_edges = set()
    eshapes = []
    dtypes = []
    for Mnode_fr in nodes:
        for Mnode_to in seq[Mnode_fr]:
            edge = edges[Mnode_fr, Mnode_to]
            if edge.ms_type == MatrixStoreTypes.SCALAR:
                scalar_edges.add((Mnode_fr, Mnode_to))
            elif edge.ms_type == MatrixStoreTypes.DIAGONAL:
                eshapes.append(edge.matrix.shape[:-1])
                dtypes.append(edge.matrix.dtype)
            else:
                eshapes.append(edge.matrix.shape[:-2])
                dtypes.append(edge.matrix.dtype)
    shape_bc = broadcast_shapes(eshapes)
    dtype_bc = np.find_common_type([], dtypes)

    if node_sizes is None:
        # this loop broadcasts all edges to a common size, as well as filters to
        # only include edges represented in the seq/req edges
        # it also establishes the vector sizes at each node
        check_sizes = True
        edgesBcast = dict()
        node_sizes = dict()
        for n_fr in nodes:
            sset = seq[n_fr]
            for n_to in sset:
                if (n_fr, n_to) in scalar_edges:
                    continue

                edge = edges[n_fr, n_to]

                if edge.ms_type == MatrixStoreTypes.SCALAR:
                    assert False
                elif edge.ms_type == MatrixStoreTypes.DIAGONAL:
                    matrix = edge.matrix
                    edgesBcast[n_fr, n_to] = np.diag(
                        np.broadcast_to(matrix, shape_bc + matrix.shape[-1:])
                    )
                    fsize = node_sizes.setdefault(n_fr, matrix.shape[-1])
                    tsize = node_sizes.setdefault(n_to, matrix.shape[-1])
                    if check_sizes:
                        assert fsize == matrix.shape[-1]
                        assert tsize == matrix.shape[-1]
                elif edge.ms_type == MatrixStoreTypes.MATRIX:
                    matrix = edge.matrix
                    edgesBcast[n_fr, n_to] = np.broadcast_to(
                        matrix, shape_bc + matrix.shape[-2:]
                    )
                    fsize = node_sizes.setdefault(n_fr, matrix.shape[-1])
                    tsize = node_sizes.setdefault(n_to, matrix.shape[-2])
                    if check_sizes:
                        assert fsize == matrix.shape[-1]
                        assert tsize == matrix.shape[-2]
    else:
        edgesBcast = dict()
        for n_fr in node_sizes.keys():
            sset = seq[n_fr]
            for n_to in sset:
                edge = np.asarray(edges[n_fr, n_to].matrix)
                edgesBcast[n_fr, n_to] = np.broadcast_to(
                    edge, shape_bc + edge.shape[-2:]
                )

    for (n_fr, n_to) in scalar_edges:
        edge = edges[n_fr, n_to].matrix
        size_fr = node_sizes[n_fr]
        size_to = node_sizes[n_to]
        assert size_fr == size_to
        edge = np.broadcast_to(np.eye(size_fr) * edge, shape_bc + (size_fr, size_to))
        edgesBcast[n_fr, n_to] = edge

    if node_order is not None:
        node_list = node_order
    else:
        node_list = list(node_sizes.keys())

    # enumerate the intern vector space of the full matrix
    node_offset_cnt = 0
    node_offsets = dict()
    for node in node_list:
        node_offsets[node] = node_offset_cnt
        node_offset_cnt += node_sizes[node]
    dense_size = node_offset_cnt

    # metamat stores the large block matrix to invert
    # it is assembled using the node offsets and counts established above, and
    # the edge/matrix representation
    # this could alternatively be done using scipy.sparse representations
    if metamat is None:
        metamat = np.zeros(shape_bc + (dense_size, dense_size), dtype=dtype_bc)

    # populate the dense submatrix
    for (n_fr, n_to), edge in edgesBcast.items():
        size_fr = node_sizes[n_fr]
        size_to = node_sizes[n_to]
        offset_fr = node_offsets[n_fr]
        offset_to = node_offsets[n_to]
        metamat[
            ..., offset_to : offset_to + size_to, offset_fr : offset_fr + size_fr,
        ] = edge
    # free some memory perhaps
    del edgesBcast

    # now perform the inversion, this could also be done using KLU, although
    # in principle it is dense and KLU's methods don't accelerate it
    if shape_bc == ():
        imetamat = scipy.linalg.inv(metamat, overwrite_a=True, check_finite=True)
    else:
        imetamat = np.linalg.inv(metamat)
    del metamat

    seq2 = collections.defaultdict(set)
    req2 = collections.defaultdict(set)
    edges2 = dict()

    # now demux the backing-store matrix into the output
    for n_fr in inputs:
        size_fr = node_sizes[n_fr]
        offset_fr = node_offsets[n_fr]
        for n_to in outputs:
            size_to = node_sizes[n_to]
            offset_to = node_offsets[n_to]
            seq2[n_fr].add(n_to)
            req2[n_to].add(n_fr)
            edges2[n_fr, n_to] = DeferredMatrixStore(
                ms_type=MatrixStoreTypes.MATRIX,
                matrix=imetamat[
                    ...,
                    offset_to : offset_to + size_to,
                    offset_fr : offset_fr + size_fr,
                ],
                needs_build=False,
            )
    return seq2, req2, edges2
