# -*- coding: utf-8 -*-
# Code originally from phasor2, Lee McCuller (Apache license v2.0)
# modified to include some additional documentation
"""
"""


def SRE_graph(
    fname, SRE, verbose=False,
):
    """
    Plotting routine to see the digraph of a matrix
    """
    seq, req, edges = SRE
    return SRABGE_graph(
        fname, (seq, req, dict(), dict(), dict(), edges), verbose=verbose,
    )


def SRABGE_graph(
    fname, SRABGE, verbose=False,
):
    import pygraphviz as pgv

    seq, req, req_alpha, seq_beta, seq_gamma, edge_map, = SRABGE

    G = pgv.AGraph(directed=True, rankdir="LR",)

    def keyfunc(v):
        if isinstance(v, tuple):
            return ",\n".join(str(i) for i in v)
        return v

    def namefunc(v):
        if isinstance(v, tuple):
            return ",\n".join(namefunc(i) for i in v)
        return str(v)

    nodes = set()
    edges = set()
    for m_to in sorted(req_alpha, key=keyfunc):
        nodes.add(m_to)
        rset = req_alpha[m_to]
        for m_fr in sorted(rset, key=keyfunc):
            nodes.add(m_fr)
            edges.add((m_fr, m_to))

    for m_fr in sorted(seq, key=keyfunc):
        nodes.add(m_fr)
        sset = seq[m_fr]
        for m_to in sorted(sset, key=keyfunc):
            edges.add((m_fr, m_to))
            nodes.add(m_to)

    for m_fr in sorted(seq_beta, key=keyfunc):
        nodes.add(m_fr)
        sset = seq_beta[m_fr]
        for m_to in sorted(sset, key=keyfunc):
            edges.add((m_fr, m_to))
            nodes.add(m_to)

    for m_fr in sorted(seq_gamma, key=keyfunc):
        nodes.add(m_fr)
        sset = seq_gamma[m_fr]
        for m_to in sorted(sset, key=keyfunc):
            edges.add((m_fr, m_to))
            nodes.add(m_to)

    for node in sorted(nodes, key=namefunc):
        if isinstance(node, tuple):
            if node[0] == "INPUT":
                G.add_node(namefunc(node), color="blue")
            if node[0] == "OUTPUT":
                G.add_node(namefunc(node), color="red")
    for m_fr, m_to in sorted(edges, key=namefunc):
        G.add_edge(namefunc(m_fr), namefunc(m_to))
    if fname is not None:
        G.draw(fname, prog="dot")
    return G
