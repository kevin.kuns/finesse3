"""GUI example using interferometer graph from finesse_graph_tmp.py"""

from finesse_graph_tmp import get_test_graph
from layout.graphviz import Graphviz
from view.qt import QtCanvas

graph = get_test_graph()
canvas = QtCanvas()

gv = Graphviz(graph, canvas)
gv.layout(prog="neato", args="") # 'neato'|'dot'|'twopi'|'circo'|'fdp'

canvas.show()
