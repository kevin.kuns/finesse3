"""Base classes for layout managers

A layout manager must take as input a graph representing an interferometer and
a canvas upon which to draw, and call out to the canvas to draw representations
of the interferometer elements. Here the components of and connections between
components within the interferometer can be translated into straight lines,
curves, right angles, or anything else as desired. Callbacks should also be
attached to GUI events such as mouse clicks via this interface, to allow the GUI
to access certain data from the underlying interferometer graph.
"""

import abc


class BaseLayout(metaclass=abc.ABCMeta):
    """Abstract methods common to all layout routines

    Attributes
    ----------
    graph : :class:`graph`
        graph representing interferometer
    canvas : :class:`canvas`
        canvas to draw primitives onto
    """

    def __init__(self, graph, canvas):
        """Instantiate base layout"""

        self.graph = graph
        self.canvas = canvas

    @abc.abstractmethod
    def layout(self):
        """Get representation of the graph in the form of geometric primitives"""
        return NotImplemented
