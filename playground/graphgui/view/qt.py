"""Qt canvas, upon which widgets are drawn"""

import sys
import itertools

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QGraphicsScene,
    QGraphicsView,
    QGraphicsEllipseItem,
    QGraphicsTextItem,
)
from PyQt5.QtGui import QPainter, QPainterPath


class QtCanvas:
    # render hints for QGraphicsView
    RENDER_HINTS = (
        QPainter.Antialiasing
        | QPainter.TextAntialiasing
        | QPainter.SmoothPixmapTransform
        | QPainter.HighQualityAntialiasing
    )

    def __init__(self):
        self.init_ui()

    def init_ui(self):
        # create application
        self.app = QApplication(sys.argv)
        self.main_window = QMainWindow()
        self.main_window.setGeometry(150, 150, 700, 700)  # x, y, w, h

        # set close behaviour to prevent zombie processes
        self.main_window.setAttribute(Qt.WA_DeleteOnClose, True)

        # create drawing area
        self.scene = QGraphicsScene()

        # create view
        self.view = QGraphicsView(self.scene, self.main_window)
        self.view.setRenderHints(self.RENDER_HINTS)
        self.view.setSceneRect(self.scene.sceneRect())
        self.view.setFixedSize(700, 700)

        # set window title
        self.main_window.setWindowTitle("Example")

    def show(self):
        self.main_window.show()

        sys.exit(self.app.exec_())

    def draw_ellipse(
        self, x_center, y_center, width, height, label=None, callbacks=None
    ):
        """Draws an ellipse"""

        # convert ellipse coordinates to lower left corner
        x_ellipse = x_center - width / 2
        y_ellipse = y_center - height / 2

        # ellipse
        ellipse = QGraphicsEllipseItem(x_ellipse, y_ellipse, width, height)

        # create label even if it's empty, just in case we want to add some
        # ability to add text graphically later
        if label is None:
            label = ""

        text = QGraphicsTextItem(label)

        # convert text coordinates to lower left corner
        x_text = x_center - text.boundingRect().width() / 2
        y_text = y_center - text.boundingRect().height() / 2

        text.setPos(x_text, y_text)

        self.scene.addItem(ellipse)
        self.scene.addItem(text)

    def draw_bezier(self, parameters, callbacks=None):
        """Draws a Bézier curve

        The parameters must consist of the control points of the Bézier curve,
        as a sequence of collections containing two elements, representing the
        x- and y-coordinates.
        """

        # TODO: add link name along path of the curve
        # see https://wiki.qt.io/Technical_FAQ#How_do_I_make_text_follow_the_line.2Fcurve_and_angle_of_the_QPainterPath.3F

        path = QPainterPath()

        # FIXME: check that "e," is always specified (this is the end point;
        # see https://www.graphviz.org/doc/info/attrs.html#k:splineType)

        # move to end point on curve
        path.moveTo(*parameters[0])

        # take three points at a time and draw Bezier curves
        # reverse order because we start at the end
        for three_tuple in self._threewise(reversed(parameters[1:])):
            # collapse coordinates into single list and draw Bezier curve
            path.cubicTo(*[c for sub in three_tuple for c in sub])

        self.scene.addPath(path)

    def _threewise(self, iterable):
        """Split iterable into a list of tuples containing groups of three
        subsequent elements

        i.e. [A, B, C, D, E] becomes [[A, B, C], [B, C, D], [C, D, E]]
        """

        # create three copies of iterable
        a, b, c = itertools.tee(iterable, 3)

        # advance b index by one
        next(b, None)

        # advance c index by two
        next(c, None)
        next(c, None)

        # merge together
        # only combines lists until any list reaches its end, then stops
        return zip(a, b, c)
