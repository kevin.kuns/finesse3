# Finesse 3

Finesse 3 is a Python based interferometer simulation software. It is currently in an early
development stage. For modelling interferometers we currently recommend that you use our mature packages:
* Finesse 2 http://www.gwoptics.org/finesse
* Pykat http://www.gwoptics.org/pykat

At the same time we invite participation to the development of Finesse. You can find the Finesse 3 resources at:
* Source code, issue tracker etc. https://git.ligo.org/finesse/finesse3
* Documentation https://finesse.docs.ligo.org/finesse3/

## Installation

For guides on the installation of Finesse 3 see: https://finesse.docs.ligo.org/finesse3/getting_started/install/index.html
