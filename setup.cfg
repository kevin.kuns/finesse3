[metadata]
name = finesse
author_email = finesse-support@nikhef.nl
license = GPL
license_file = LICENSE.md
url = https://www.gwoptics.org/finesse
description = Simulation tool for modelling laser interferometers
long_description = file: README.md
long_description_content_type = text/markdown
classifiers =
    Development Status :: 3 - Alpha
    Topic :: Scientific/Engineering :: Physics
    Intended Audience :: Science/Research
    Natural Language :: English
    License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
    Operating System :: Microsoft :: Windows
    Operating System :: POSIX
    Operating System :: Unix
    Operating System :: MacOS
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9
    Topic :: Scientific/Engineering :: Physics
project_urls =
    Bug Tracker = https://git.ligo.org/finesse/finesse3/issues
    Source Code = https://git.ligo.org/finesse/finesse3
    Documentation = https://finesse.docs.ligo.org/finesse3

[options]
package_dir =
    = src
packages = find:
zip_safe = false
python_requires = >=3.8
install_requires =
    # See https://finesse.docs.ligo.org/finesse3/developer/codeguide/requirements.html
    # for more information on how to set requirements.
    # Note to package maintainers: these versions may not be the absolute minimum
    # requirements; it may be possible to reduce them if they are problematic for
    # packaging - get in touch with developers.
    numpy >= 1.20  # Must match numpy in pyproject.toml build-system requirements.
    scipy >= 1.4
    matplotlib >= 3.5
    networkx >= 2.4
    sly >= 0.4
    click >= 7.1
    click-default-group >= 1.2.2
    tabulate >= 0.8.7
    control >= 0.9
    sympy >= 1.6
    more_itertools
    tqdm
    h5py
    deprecated

[options.packages.find]
where=src

[options.package_data]
finesse = finesse.ini, usr.ini.dist
finesse.plotting.style = *.mplstyle

[options.entry_points]
console_scripts =
    kat3 = finesse.__main__:cli
pygments.lexers =
    KatScript = finesse.script.highlighter:KatScriptPygmentsLexer
    KatScriptInPython = finesse.script.highlighter:KatScriptSubstringPygmentsLexer

[options.extras_require]
# Optional extras. These should not be pinned to specific versions so that we can
# continuously test the latest versions. One exception would be for when there are
# incompatibilities with new versions, in which case pin to a working version and leave
# a comment as to why.
test =
    pytest
    pytest-cov
    coverage[toml] == 4.5.4  # Cython incompatibility with newer versions: https://github.com/cython/cython/issues/3515
    Faker
    hypothesis
    pycobertura
    pyspellchecker
docs =
    sphinx
    sphinx_rtd_theme
    sphinxcontrib-bibtex
    sphinxcontrib-katex
    sphinxcontrib-svg2pdfconverter
    sphinxcontrib-programoutput
    jupyter-sphinx
    numpydoc
    reslate
lint =
    black
    pre-commit
    pylint
    flake8
    flake8-bugbear
    doc8
inplacebuild =
    # Dependencies required to be able to rebuild Finesse extensions in-place, e.g.
    # using `make`. See docs for info about why this exists:
    # https://finesse.docs.ligo.org/finesse3/developer/setting_up.html#rebuilding-extensions.
    # These requirements (and versions, if present) must match those in pyproject.toml
    # build-system requirements.
    cython
    setuptools_scm[toml]
    tqdm
    h5py
graphviz =
    pygraphviz

[flake8]  # Annoyingly, flake8 doesn't support pyproject.toml so we have to put this here...
# Ignored rules.
ignore =
    # Errors.
    E203 # Whitespace before ':'
    E266 # Too many leading '#' for block comment
    E402 # Module level import not at top of file
    E501 # Line too long
    E731 # Do not assign a lambda expression, use a def
    E741 # ambiguous variable name
    E231 # missing whitespace after ','
    # Warnings.
    W503 # line break before binary operator (soon deprecated; see https://www.flake8rules.com/rules/W503.html)

# Excluded directories.
exclude =
    .git
    __pycache__
    src/docs/source/conf.py
    build
    dist
